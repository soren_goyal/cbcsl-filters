
clc; clear all;

fDatabase = '/eecf/cbcsl/data04/ram/AU Detector/Faster/Emotion Database/Shape_Features/Database';

list = dir([fDatabase '/jpgimages/*.jpg']);          % List of images
filesMarkings = dir([fDatabase '/markings/*.mat']);  % List markings

matrix_ones=ones(66,66);
M_upper=triu(matrix_ones);
% M_vect=nonzeros(M_upper);
k=find(~M_upper);

Ni = length(list);    
shape_features=[];
for i = 1:size(filesMarkings)
    display(i);
    load([fDatabase '/markings/'  filesMarkings(i).name]);
    x=faceCoordinatesUnwarped;
    y=faceCoordinatesUnwarped;
    
    D=pdist2(x,y);
    D_upper=triu(D);
    D_vector=nonzeros(D_upper)';
    
    if size(D_vector,2)~=((66*65)/2)
        for m1=1:66
            for m2=m1+1:66
                if D_upper(m1,m2)==0
                    D_upper(m1,m2)=1;
                end
            end
        end
      D_vector=nonzeros(D_upper)';   
    end
    
    shape_features=vertcat(shape_features,D_vector);
end

save('ShapeFeatures', 'shape_features');



