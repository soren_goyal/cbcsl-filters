%Build Delaunay Triangulation for one region of two faces and normalize
%Updated 09/19/14

%input:
%   facecor: faceCoordinatesUnwarped 78x2 double
%   pointsnum: points labels that matters

function [transformedTRI1,transformedTRI2,TRIfeature]=transformTRI(facecor1,facecor2,pointnum)

leftfaceavg1=[mean(facecor1(64:70,1)),mean(facecor1(64:70,1))];
rightfaceavg1=[mean(facecor1(72:78,1)),mean(facecor1(72:78,1))];

leftfaceavg2=[mean(facecor2(64:70,1)),mean(facecor2(64:70,1))];
rightfaceavg2=[mean(facecor2(72:78,1)),mean(facecor2(72:78,1))];

avgfacewidth1=norm(leftfaceavg1-rightfaceavg1);
avgfacewidth2=norm(leftfaceavg2-rightfaceavg2);

for i=1:length(pointnum)
    x=facecor1(pointnum(i),1);
    y=facecor1(pointnum(i),2);
    facepart1(i,1)=x;
    facepart1(i,2)=y;
    
    x=facecor2(pointnum(i),1);
    y=facecor2(pointnum(i),2);
    facepart2(i,1)=x;
    facepart2(i,2)=y;
end

TRI1=delaunay(facepart1(:,1),facepart1(:,2));
T1=triangulation(TRI1,facepart1(:,1),facepart1(:,2));
E1=edges(T1);
% TRI2=delaunay(facepart2(:,1),facepart2(:,2));
T2=triangulation(TRI1,facepart2(:,1),facepart2(:,2));
E2=edges(T2);

for i=1:size(E1,1)
    E1(i,3)=norm(facepart1(E1(i,1),:)-facepart1(E1(i,2),:))*avgfacewidth2/avgfacewidth1;
end
for i=1:size(E2,1)
    E2(i,3)=norm(facepart2(E2(i,1),:)-facepart2(E2(i,2),:));
end

transformedTRI1.points=TRI1;
transformedTRI1.edges=E1;
transformedTRI2.points=TRI1;
transformedTRI2.edges=E2;
TRIfeature=E1(:,3)-E2(:,3);
