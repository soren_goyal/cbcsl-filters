%Build Delaunay Triangulation 

function [Tf]=TRIfeatures(fDatabase, useful_markings)

%% Load data
list = dir([fDatabase '/jpgimages/*.jpg']);          % List of images
filesMarkings = dir([fDatabase '/markings/*.mat']);  % List markings

Ni = length(list);                                   % Number of images

fprintf('Number of images: %d\n',Ni)
disp(['Database ' fDatabase])
    load('01_109_2454.mat');

    facecoord1 = faceCoordinatesUnwarped(useful_markings,1);
    facecoord2 = faceCoordinatesUnwarped(useful_markings,2);
    TRI=delaunay(facecoord1,facecoord2);
    T=triangulation(TRI,facecoord1,facecoord2);
    E=edges(T);

for i = 1:Ni
    load([fDatabase '/markings/'  filesMarkings(i).name]);
    display(i);
    
    facecoord1 = faceCoordinatesUnwarped(useful_markings,1);
    facecoord2 = faceCoordinatesUnwarped(useful_markings,2);
    

    kk=0;
    for j=1:size(T,1)
        p1=[facecoord1(T(j,1)) facecoord2(T(j,1))];
        p2=[facecoord1(T(j,2)) facecoord2(T(j,2))];
        p3=[facecoord1(T(j,3)) facecoord2(T(j,3))];
        
        kk=kk+1;
        v1=p2-p1;
        v2=p3-p1;
        nv1=max(norm(v1),1e-5);
        nv2=max(norm(v2),1e-5);
        Tf(i,kk)=acos(dot(v1,v2)/nv1/nv2);
        
        kk=kk+1;
        v1=p1-p2;
        v2=p3-p2;
        nv1=max(norm(v1),1e-5);
        nv2=max(norm(v2),1e-5);
        Tf(i,kk)=acos(dot(v1,v2)/nv1/nv2);
        
        kk=kk+1;
        v1=p1-p3;
        v2=p2-p3;
        nv1=max(norm(v1),1e-5);
        nv2=max(norm(v2),1e-5);
        Tf(i,kk)=acos(dot(v1,v2)/nv1/nv2);
    end
end



function m=getmod(m,k)
    if mod(m,k)==0
        m=k;
    else
        m=mod(m,k);
    end
