function [I_resize,faceCoordinatesUnwarped]= resize(I_location,markings_location)

markings_struct=load(markings_location);
markings=markings_struct.faceCoordinatesUnwarped_orig;

eye_distance_mean=200;
eye_distance_image=pdist2(markings(20,:),markings(29,:));

I=imread(I_location);
% % %  Cropping Face

tp1=mean([markings(3,2),markings(8,2)])-eye_distance_image;
if tp1<0
    ylim_upper=1;
else
    ylim_upper=tp1;
end


tp2=mean([markings(58,2),markings(59,2)])+(eye_distance_image/2);

if tp2>size(I,1)
    ylim_lower=size(I,1);
else
    ylim_lower=tp2;
end

height=ylim_lower - ylim_upper;

tp3=markings(51,1)-(eye_distance_image/3);

if tp3<0
    xlim_left=1;
else
    xlim_left=tp3;
end

tp4=markings(65,1)+(eye_distance_image/3);
if tp4>size(I,2)
    xlim_right=size(I,2);
else
    xlim_right=tp4;
end

width=xlim_right - xlim_left;
% 
I_crop=imcrop(I,[xlim_left,ylim_upper,width,height]);

markings_crop=[(markings(:,1)-xlim_left),(markings(:,2)-ylim_upper)];

% % %  Resizing image - eye distance

scale=eye_distance_mean/eye_distance_image;

I_resize=imresize(I_crop,scale);
markings_resize=markings_crop*scale;
faceCoordinatesUnwarped=markings_resize;

% % % hold on
% % % set(gca,'YDir','reverse');
% % % 
% % % imagesc(I_resize);
% % % plot(faceCoordinatesUnwarped(:,1),faceCoordinatesUnwarped(:,2),'.');
% % % 
% % % hold off


        