function gaborResponse = GaborFeaturesFunc_ver2(I,faceCoordinatesUnwarped,useful_markings)
% This function extract the response of Gabor filters at different locations (landmarks)
%
% fDatabase:        Database used for experiments
% fGaborResponses:  Folder that contains the Gabor responses for the database fDatabase
% useful_markings:  Used landmarks for the experiments


%% Generate filter bank
filterBank = GenerateFilters();  % Generate gabor filters
filterBank1 = filterBank{1};     
filterBank2 = filterBank{2};

Nfilters = size(filterBank1,3); % Number of filters




%% Main loop
Gf=NaN*zeros(1,length(useful_markings)*80);
try

    if size(I,3)==3
        I = rgb2gray(uint8(I)); % Load image
    end

    
    resp = [];
    Wr=[]; Wi=[]; gIr=[]; gIi=[]; gI=[];
    
    %% Filter images using a parallel loop
    
    
    
    %% Find Gabor responses around a landmark
    % Load markings
    
     %faceCoordinatesUnwarped = lmk.faceCoordinatesUnwarped(useful_markings,:);
     %faceCoordinatesUnwarped = lmk.tmp_markings(useful_markings,:);
    numRows = size(I,1);
    numCols = size(I,2);
    kk=0;
    for k=1:length(useful_markings)      % k-th landmark
        %%% Extract gabor responses of k-th landmark
        row = fix(faceCoordinatesUnwarped(k,2));
        col = fix(faceCoordinatesUnwarped(k,1));

        rows = linspace(max(row-37,1),min(row+37,numRows),75);
        cols = linspace(max(col-37,1),min(col+37,numCols),75);

        I2 = I(floor(rows),:);
        I3 = I2(:,floor(cols));
        resp = zeros(1,Nfilters);

        for f=1:Nfilters                          

            gIr = sum(sum(double(I3).*filterBank1(:,:,f)));
%             gIr = imfilter(I, Wr, 'symmetric');

%             Wi = filterBank2(:,:,f);
            gIi = sum(sum(double(I3).*filterBank2(:,:,f)));
%             gIi = imfilter(I, Wi, 'symmetric');

            gI = double(gIr) + sqrt(-1)*double(gIi);  % Complex response
            resp(f) = gI;
            r = gI;
            
            kk = kk+1;
            Gf(kk) = abs(r);     % Extract magnitude
            
            kk = kk+1;
            %Gf(i,kk) = phase(r);   % Extract phase
            Gf(kk) = angle(r);
        end        
        
        
    end
    gaborResponse = Gf(:);

catch ME
    disp(ME);
    gaborResponse = [];
end
    %% Save data    

