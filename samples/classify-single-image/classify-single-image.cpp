#include <iostream>
#include <cbcsl-utility.h>
#include <delaunay.h>
#include <distances.h>
#include <gabor-features.h>
#include <classifier.h>
#include <vector>

using namespace std;
using namespace cv;

int main(int argc, char *argv[])
{
	if(!(argc == 4 || argc == 5))
 	{
	    cout<<"Invalid Arguments\n\n";
	    cout<<"./classify-single-image <image-name> <landmarks-file-name> <model-folder> \n";
	    cout<<"\timage file\t- any commonly used image format .jpg, .png, .bmp, etc\n";
	    cout<<"\tlandmarks file\t- each line fo the file should contain the point as \"x	y\" without the inverted commas\n";
	    cout<<"\tmodel folder\t - The folder should contain the files - C.txt, dd.txt, K1.txt, K2.txt, nc.txt, trainingdata, v.txt. The name should be wthoout the forward slash.";
	    cout<<"To Run in debug mode-"<<endl;
	    cout<<"./classify-single-image <imagename> <landmarks file> <model-folder> d\n";
    	return 0; 
  	}
	char * imagename = argv[1]; 
	char * landmarkname = argv[2];
	char * model_folder = argv[3];
	bool debug_mode = false;
	if(argc == 5)
		debug_mode = true;

	Mat src = imread( imagename, CV_LOAD_IMAGE_GRAYSCALE);
	if(debug_mode)
		if(src.empty())
			cout<<"Unable to read:"<<imagename<<endl;
		else
		cout<<"Read "<<imagename<<" as GRAYSCALE"<<endl;

	vector<Point2f> landmarks = readLandmarkPoints(landmarkname);
	if(debug_mode)
	{
		if(landmarks.empty())
		{
		cout<<"No Landmark Points read"<<endl;
		return 0;
		}
		cout<<"Read "<<landmarks.size()<<" points from "<< landmarkname<<endl;
		for (vector<Point2f>::iterator it = landmarks.begin() ; it != landmarks.end(); ++it)
		cout<<(*it).x<<"\t"<<(*it).y<<endl;
	}
	getFace(src, src, landmarks);
	adjustLandmarks(landmarks, landmarks);
	visualizePointsOnImage(src, landmarks);
  	string filename;
    //Read DD
	filename.assign(model_folder);
	filename =  filename + "/dd.txt";
	Mat DD;
	readCSVToMat(DD, filename);
	if(debug_mode)
	{
		if(DD.empty())
			cout<<"Either "<<filename<<" was not found or the file is empty"<<endl;
		else
			cout<<"Read "<<filename<<" of size "<<DD.rows<<"x"<<DD.cols<<endl;
	}
	//Read K1
	filename.assign(model_folder);
	filename =  filename + "/K1.txt";
	Mat K1;
	readCSVToMat(K1, filename);
	if(debug_mode)
	{
		if(K1.empty())
			cout<<"Either "<<filename<<" was not found or the file is empty"<<endl;
		else
			cout<<"Read "<<filename<<" of size "<<K1.rows<<"x"<<K1.cols<<endl;
	}
	//Read NC
	filename.assign(model_folder);
	filename =  filename + "/nc.txt";
	Mat NC_mat;
	readCSVToMat(NC_mat, filename);
	if(debug_mode)
	{
		if(NC_mat.empty())
			cout<<"Either "<<filename<<" was not found or the file is empty"<<endl;
		else
			cout<<"Read "<<filename<<" of size "<<NC_mat.rows<<"x"<<NC_mat.cols<<endl;
	}
	Mat NC(1,2, CV_32S);
	NC.at<int>(0,0) = (int)NC_mat.at<float>(0,0);
	NC.at<int>(0,1) = (int)NC_mat.at<float>(0,1);
	//Read V
	filename.assign(model_folder);
	filename =  filename + "/v.txt";
	Mat V;
	readCSVToMat(V, filename);
	if(debug_mode)
	{
		if(V.empty())
			cout<<"Either "<<filename<<" was not found or the file is empty"<<endl;
		else
			cout<<"Read "<<filename<<" of size "<<V.rows<<"x"<<V.cols<<endl;
	}
	//Read C
	filename.assign(model_folder);
	filename =  filename + "/C.txt";
	Mat C_mat;
	readCSVToMat(C_mat, filename);
	if(debug_mode)
	{
		if(C_mat.empty())
			cout<<"Either "<<filename<<" was not found or the file is empty"<<endl;
		else
			cout<<"Read "<<filename<<" of size "<<C_mat.rows<<"x"<<C_mat.cols<<endl;
	}
	int C = (int)C_mat.at<float>(0,0);
	//Read Op Sigma
	filename.assign(model_folder);
	filename =  filename + "/op_sigma.txt";
	Mat OPSIGMA_mat;
	readCSVToMat(OPSIGMA_mat, filename);
	if(debug_mode)
	{
		if(OPSIGMA_mat.empty())
			cout<<"Either "<<filename<<" was not found or the file is empty"<<endl;
		else
			cout<<"Read "<<filename<<" of size "<<OPSIGMA_mat.rows<<"x"<<OPSIGMA_mat.cols<<endl;
	}
	float OPSIGMA = OPSIGMA_mat.at<float>(0,0);

	//Read TRAININGDATA
	filename.assign(model_folder);
	filename =  filename + "/trainingdata.txt";
	Mat TRAININGDATA;
	readCSVToMat(TRAININGDATA, filename);
	if(debug_mode)
	{
		if(TRAININGDATA.empty())
			cout<<"Either "<<filename<<" was not found or the file is empty"<<endl;
		else
			cout<<"Read "<<filename<<" of size "<<TRAININGDATA.rows<<"x"<<TRAININGDATA.cols<<endl;
	}
	vector<float> distances;
	DelaunayFeatures delaunay(src, landmarks);
	GaborFeatures gabor;
  	int n = gabor.generateFilters();
  	if(debug_mode)
    	cout<<"Number of Filters Generated:"<<gabor.filters.size()<<endl;
  	gabor.applyFilters(src, landmarks);
	delaunay.calcDelaunayIndices();
	delaunay.calcDelaunayAngles();
	distances = getEuclideanDistances(landmarks);

	vector<float> input;
	for(int i = 0; i < gabor.responses.size(); i++)
		input.push_back(gabor.responses[i]);
	for(int i = 0; i < delaunay.alist.size(); i++)
	{
		input.push_back(delaunay.alist[i][0]);
		input.push_back(delaunay.alist[i][1]);
		input.push_back(delaunay.alist[i][2]);
	}
	for(int i = 0; i < distances.size(); i++)
		input.push_back(distances[i]);
	Mat unlabelledvector(input.size(), 1, CV_32F);
	for(int i = 0; i < input.size(); i++)
		unlabelledvector.at<float>(i,0) = input[i];

	Classifier classifier;
	classifier.setV(V);
	classifier.setC(C);
	classifier.setNC(NC);
	classifier.setK1(K1);
	classifier.setOpSigma(OPSIGMA);
	classifier.setLabelledVectors(TRAININGDATA);
	Mat pred = classifier.classify(unlabelledvector);
	cout<<pred<<endl;
	return 0;
}
