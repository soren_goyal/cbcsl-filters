#include <cbcsl-utility.h>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

using namespace cv;
using namespace std;

int main(int argc, char *argv[])
{
	//Parse Arguments
  if(!(argc == 4 || argc == 5))
  {
    cout<<"Invalid Arguments\n\n";
    cout<<"./get-face <image file> <landmark file> <output>\n"<<"Sample use:\n./get-face lena.jpg landmarks.dat rot-lena.jpg\n";
    cout<<"\timage file\t- any commonly used image format .jpg, .png, .bmp, etc\n";
    cout<<"\tlandmarks file\t- each line fo the file should contain the point as \"x	y\" without the inverted commas\n";
    cout<<"To Run in debug mode-"<<endl;
    cout<<"./get-face <image file> <landmark file> <output> d\n";
    return 0;
  }
  char * imagename = argv[1];
  char * landmarkname = argv[2];
  char *outputname = argv[3];
  bool debug_mode = false;
  if(argc == 5)
    debug_mode = true;
  /*******************************************************************************************************/
	//Load files into memory
	Mat src = imread( imagename, CV_LOAD_IMAGE_GRAYSCALE);
	if(debug_mode)
	if(src.empty())
		cout<<"Unable to read:"<<imagename<<endl;
	else
		cout<<"Read "<<imagename<<" as GRAYSCALE"<<endl;

	vector<Point2f> landmarks = readLandmarkPoints(landmarkname);
	if(debug_mode)
	if(landmarks.empty())
	{
		cout<<"No Landmark Points read"<<endl;
		return 0;
	}

	if(debug_mode)
	{
		cout<<"Read "<<landmarks.size()<<" points from "<< landmarkname<<endl;
		for (vector<Point2f>::iterator it = landmarks.begin() ; it != landmarks.end(); ++it)
		cout<<(*it).x<<"\t"<<(*it).y<<endl;
	}
	/*******************************************************************************************************/
	Mat dst;
	getFace(src, dst, landmarks);
	adjustLandmarks(landmarks, landmarks);

	visualizePointsOnImage(dst, landmarks);
	imwrite( outputname, dst);
	for (vector<Point2f>::iterator it = landmarks.begin() ; it != landmarks.end(); ++it)
		cout<<(*it).x<<"\t"<<(*it).y<<endl;

	return 0;
}
