#include <iostream>
#include <cbcsl-utility.h>
#include <delaunay.h>
#include <distances.h>
#include <gabor-features.h>
#include <classifier.h>

using namespace std;
using namespace cv;

int main(int argc, char *argv[])
{
	if(!(argc == 2 || argc == 3))
 	{
	    cout<<"Invalid Arguments\n\n";
	    cout<<"./classifier-test <model-folder> \n";
	    cout<<"\tmodel folder\t - The folder should contain the files - C.txt, dd.txt, K1.txt, K2.txt, nc.txt, trainingdata, v.txt. The name should be wthoout the forward slash.";
	    cout<<"To Run in debug mode-"<<endl;
	    cout<<"/classifier-test <model-folder> d\n";
    	return 0; 
  	}
  	
	
	char * model_folder = argv[1];
	bool debug_mode = false;
	if(argc == 3)
		debug_mode = true;

	string filename;
	
	//Read DD
	filename.assign(model_folder);
	filename =  filename + "/dd.txt";
	Mat DD;
	readCSVToMat(DD, filename);
	if(debug_mode)
	{
		cout<<"Attempted Reading "<<filename<<endl;;
		if(DD.empty())
			cout<<"Either "<<filename<<" was not found or the file is empty"<<endl;
		else
			cout<<"Read "<<filename<<" of size "<<DD.rows<<"x"<<DD.cols<<endl;
	}
	//Read K1
	filename.assign(model_folder);
	filename =  filename + "/K1.txt";
	Mat K1;
	readCSVToMat(K1, filename);
	if(debug_mode)
	{
		cout<<"Attempted Reading "<<filename<<endl;;
		if(K1.empty())
			cout<<"Either "<<filename<<" was not found or the file is empty"<<endl;
		else
			cout<<"Read "<<filename<<" of size "<<K1.rows<<"x"<<K1.cols<<endl;
	}
	//Read K2
	filename.assign(model_folder);
	filename =  filename + "/K2.txt";
	Mat K2;
	readCSVToMat(K2, filename);
	if(debug_mode)
	{
		cout<<"Attempted Reading "<<filename<<endl;;
		if(K2.empty())
			cout<<"Either "<<filename<<" was not found or the file is empty"<<endl;
		else
			cout<<"Read "<<filename<<" of size "<<K2.rows<<"x"<<K2.cols<<endl;
	}
	//Read NC
	filename.assign(model_folder);
	filename =  filename + "/nc.txt";
	Mat NC_mat;
	readCSVToMat(NC_mat, filename);
	if(debug_mode)
	{
		cout<<"Attempted Reading "<<filename<<endl;;
		if(NC_mat.empty())
			cout<<"Either "<<filename<<" was not found or the file is empty"<<endl;
		else
			cout<<"Read "<<filename<<" of size "<<NC_mat.rows<<"x"<<NC_mat.cols<<endl;
	}
	Mat NC(1,2, CV_32S);
	NC.at<int>(0,0) = (int)NC_mat.at<float>(0,0);
	NC.at<int>(0,1) = (int)NC_mat.at<float>(0,1);
	//Read V
	filename.assign(model_folder);
	filename =  filename + "/v.txt";
	Mat V;
	readCSVToMat(V, filename);
	if(debug_mode)
	{
		cout<<"Attempted Reading "<<filename<<endl;;
		if(V.empty())
			cout<<"Either "<<filename<<" was not found or the file is empty"<<endl;
		else
			cout<<"Read "<<filename<<" of size "<<V.rows<<"x"<<V.cols<<endl;
	}
	//Read C
	filename.assign(model_folder);
	filename =  filename + "/C.txt";
	Mat C_mat;
	readCSVToMat(C_mat, filename);
	if(debug_mode)
	{
		cout<<"Attempted Reading "<<filename<<endl;;
		if(C_mat.empty())
			cout<<"Either "<<filename<<" was not found or the file is empty"<<endl;
		else
			cout<<"Read "<<filename<<" of size "<<C_mat.rows<<"x"<<C_mat.cols<<endl;
	}
	int C = (int)C_mat.at<float>(0,0);
	//Read Op Sigma
	filename.assign(model_folder);
	filename =  filename + "/op_sigma.txt";
	Mat OPSIGMA_mat;
	readCSVToMat(OPSIGMA_mat, filename);
	if(debug_mode)
	{
		cout<<"Attempted Reading "<<filename<<endl;;
		if(OPSIGMA_mat.empty())
			cout<<"Either "<<filename<<" was not found or the file is empty"<<endl;
		else
			cout<<"Read "<<filename<<" of size "<<OPSIGMA_mat.rows<<"x"<<OPSIGMA_mat.cols<<endl;
	}
	float OPSIGMA = OPSIGMA_mat.at<float>(0,0);

	//Read TESTINGDATA
	filename.assign(model_folder);
	filename =  filename + "/testingdata.txt";
	Mat TESTINGDATA;
	readCSVToMat(TESTINGDATA, filename);
	if(debug_mode)
	{
		cout<<"Attempted Reading "<<filename<<endl;;
		if(TESTINGDATA.empty())
			cout<<"Either "<<filename<<" was not found or the file is empty"<<endl;
		else
			cout<<"Read "<<filename<<" of size "<<TESTINGDATA.rows<<"x"<<TESTINGDATA.cols<<endl;
	}
	//Read TRAININGDATA
	filename.assign(model_folder);
	filename =  filename + "/trainingdata.txt";
	Mat TRAININGDATA;
	readCSVToMat(TRAININGDATA, filename);
	if(debug_mode)
	{
		cout<<"Attempted Reading "<<filename<<endl;;
		if(TRAININGDATA.empty())
			cout<<"Either "<<filename<<" was not found or the file is empty"<<endl;
		else
			cout<<"Read "<<filename<<" of size "<<TRAININGDATA.rows<<"x"<<TRAININGDATA.cols<<endl;
	}

	Classifier classifier;
	classifier.setV(V);
	classifier.setC(C);
	classifier.setNC(NC);
	classifier.setK1(K1);
	classifier.setOpSigma(OPSIGMA);
	classifier.setLabelledVectors(TRAININGDATA);
	Mat pred = classifier.classify(TESTINGDATA);
	cout<<pred;

	cout<<"DD Match:"<<compareMat(classifier.dd, DD)<<endl;
	cout<<"K2 Match:"<<compareMat(classifier.K2, K2)<<endl;
	return 0;
}