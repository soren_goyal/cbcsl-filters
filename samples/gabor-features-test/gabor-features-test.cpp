#include <iostream>

#include <cbcsl-utility.h>
#include <gabor-features.h>

using namespace cv;
using namespace std;


int display( Mat &img, int delay );

int main( int argc, char** argv )
{
  //Parse Arguments
  if(!(argc == 3 || argc == 4))
  {
    cout<<"Invalid Arguments\n\n";
    cout<<"./filter <image file> <landmark file>\n"<<"Sample use:\n./filter lena.jpg landmarks.dat\n";
    cout<<"\timage file\t- any commonly used image format .jpg, .png, .bmp, etc\n";
    cout<<"\tlandmarks file\t- each line fo the file should contain the point as \"x,y\" without the inverted commas\n";
    cout<<"To Run in debug mode-"<<endl;
    cout<<"./filter <image file> <landmark file> d\n";
    return 0;
  }
  char * imagename = argv[1];
  char * landmarkname = argv[2];
  bool debug_mode = false;
  if(argc == 4)
    debug_mode = true;
  /*******************************************************************************************************/
  
  //Load files into memory
  Mat src = imread( imagename, CV_LOAD_IMAGE_GRAYSCALE);
  if(debug_mode)
    if(src.empty())
      cout<<"Unable to read:"<<imagename<<endl;
    else
      cout<<"Read "<<imagename<<" as GRAYSCALE"<<endl;

  vector<Point2f> landmarks = readLandmarkPoints(landmarkname);
  if(debug_mode)
    if(landmarks.empty())
    {
    	cout<<"No Landmark Points read"<<endl;
    	return 0;
    }

  if(debug_mode)
  {
  	cout<<"Read "<<landmarks.size()<<" points from "<< landmarkname<<endl;
  	for (vector<Point2f>::iterator it = landmarks.begin() ; it != landmarks.end(); ++it)
    	cout<<(*it).x<<"\t"<<(*it).y<<endl;
	}
  /*******************************************************************************************************/
  //Visualize Image
  if(debug_mode) visualizePointsOnImage(src, landmarks);
  /*******************************************************************************************************/
  GaborFeatures gabor;
  int n = gabor.generateFilters();
  if(debug_mode)
    cout<<"Number of Filters Generated:"<<gabor.filters.size()<<endl;
  gabor.applyFilters(src, landmarks);
  for(vector<float>::iterator it = gabor.responses.begin(); it!= gabor.responses.end(); it++)
    cout<<(*it)<<endl;
  return 0;
}
