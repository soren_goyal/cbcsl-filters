#include <iostream>

#include <cbcsl-utility.h>
#include <gabor-features.h>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

using namespace cv;
using namespace std;


int display( Mat &img, int delay );

int main( int argc, char** argv )
{
  //Parse Arguments
  if(!(argc == 4 || argc == 5))
  {
    cout<<"Invalid Arguments\n\n";
    cout<<"./gabor-filters-read-test <image file> <landmark file> <filter-folder>\n"<<"Sample use:\n./gabor-filters-read-test lena.jpg landmarks.dat data/gabor-filters\n";
    cout<<"\timage file\t- any commonly used image format .jpg, .png, .bmp, etc\n";
    cout<<"\tlandmarks file\t- each line fo the file should contain the point as \"x,y\" without the inverted commas\n";
    cout<<"\tfilter-folder\t- The Folde should contain all the filters named appropriately\n";
    cout<<"To Run in debug mode-"<<endl;
    cout<<"./gabor-filters-read-test <image file> <landmark file> <filter-folder>d\n";
    return 0;
  }
  char * imagename = argv[1];
  char * landmarkname = argv[2];
  char * foldername = argv[3];
  bool debug_mode = false;
  if(argc == 5)
    debug_mode = true;
  /*******************************************************************************************************/
  
  //Load files into memory
  Mat src = imread( imagename, CV_LOAD_IMAGE_GRAYSCALE);
  if(debug_mode)
    if(src.empty())
      cout<<"Unable to read:"<<imagename<<endl;
    else
      cout<<"Read "<<imagename<<" as GRAYSCALE"<<endl;

  vector<Point2f> landmarks = readLandmarkPoints(landmarkname);
  if(debug_mode)
    if(landmarks.empty())
    {
    	cout<<"No Landmark Points read"<<endl;
    	return 0;
    }


  if(debug_mode)
  {
  	cout<<"Read "<<landmarks.size()<<" points from "<< landmarkname<<endl;
  	for (vector<Point2f>::iterator it = landmarks.begin() ; it != landmarks.end(); ++it)
    	cout<<(*it).x<<"\t"<<(*it).y<<endl;
	}
  /*******************************************************************************************************/
  //Visualize Image
  if(debug_mode) visualizePointsOnImage(src, landmarks);
  /*******************************************************************************************************/
  GaborFeatures gabor;
  int n = gabor.generateFilters();
  if(debug_mode)
    cout<<"Number of Filters Generated:"<<gabor.filters.size()<<endl;
  vector<Mat> generatedfilters(gabor.filters);
  gabor.filters.clear();
  n = gabor.readFilters(foldername);
  if(debug_mode)
  { 
    if( n != 0 )
      cout<<"Number of Filters Read:"<<gabor.filters.size()<<endl;
    else
    {
      cout<<"Error reading file. CHECK IF THE FILE/FOLDERS EXIST OR NOT\n";
      return 0;
    }
  }
  for(int i = 0; i <generatedfilters.size(); i++)
  {
    imshow("generated filter", generatedfilters[i]);
    imshow("readfilter", gabor.filters[i]);
    waitKey(0);
    if(!compareMat(generatedfilters[i], gabor.filters[i]))
      cout<<i<<"\t:Not EQUAL\n";
  }
  gabor.applyFilters(src, landmarks);
  for(vector<float>::iterator it = gabor.responses.begin(); it!= gabor.responses.end(); it++)
    cout<<(*it)<<endl;
  return 0;
}
