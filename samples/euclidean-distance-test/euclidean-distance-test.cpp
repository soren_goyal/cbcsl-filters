#include <iostream>

#include <cbcsl-utility.h>
#include <distances.h>

using namespace cv;
using namespace std;


int display( Mat &img, int delay );

int main( int argc, char** argv )
{
  //Parse Arguments
  if(!(argc == 2 || argc == 3))
  {
    cout<<"Invalid Arguments\n\n";
    cout<<"./euclidean-distances <landmark file>\n"<<"Sample use:\n./euclidean-distances landmarks.dat\n";
    cout<<"\tlandmarks file\t- each line fo the file should contain the point as \"x\ty\" without the inverted commas\n";
    cout<<"To Run in debug mode-"<<endl;
    cout<<"./filter <landmark file> d\n";
    return 0;
  }
  char * landmarkname = argv[1];
  bool debug_mode = false;
  if(argc == 3)
    debug_mode = true;
  /*******************************************************************************************************/
  
  //Load files into memory

  vector<Point2f> landmarks = readLandmarkPoints(landmarkname);
  if(debug_mode)
    if(landmarks.empty())
    {
    	cout<<"No Landmark Points read"<<endl;
    	return 0;
    }
  if(debug_mode)
  {
  	cout<<"Read "<<landmarks.size()<<" points from "<< landmarkname<<endl;
  	for (vector<Point2f>::iterator it = landmarks.begin() ; it != landmarks.end(); ++it)
    	cout<<(*it).x<<"\t"<<(*it).y<<endl;
	}
  /*******************************************************************************************************/
  vector<float> v;
  v = getEuclideanDistances(landmarks);
  for(vector<float>::iterator it = v.begin(); it!= v.end(); it++)
    cout<<(*it)<<endl;
  return 0;
}