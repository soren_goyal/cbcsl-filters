#include <delaunay.h>

#include <iostream>
#include <cbcsl-utility.h>

using namespace cv;
using namespace std;


int display( Mat &img, int delay );

int main( int argc, char** argv )
{
  //Parse Arguments
  if(!(argc == 3 || argc == 4))
  {
    cout<<"Invalid Arguments\n\n";
    cout<<"./delaunay-test-2 <Landmark file> <Delaunay Indices file>\n"<<"Sample use:\n./delaunay-test landmarks1.dat indicies1.dat\n";
    cout<<"\tlandmarks file\t- each line fo the file should contain the point as \"x\ty\" without the inverted commas\n";
    cout<<"\tDelaunay Indices file\t- each line fo the file should contain the indices as \"a\tb\tc\" without the inverted commas\n";
    cout<<"To Run in debug mode-"<<endl;
    cout<<"./delaunay-test-2 <Landmark file> <Delaunay Indices file> d\n";
    return 0;
  }
  char * landmarkname = argv[2];
  char * indicesname = argv[3];
  bool debug_mode = false;
  if(argc == 4)
    debug_mode = true;
  /*******************************************************************************************************/
  
  //Load files into memory
  vector<Point> landmarks = readLandmarkPoints(landmarkname);
  if(debug_mode)
    if(landmarks.empty())
    {
      cout<<"No Landmark Points read"<<endl;
      return 0;
    }

  if(debug_mode)
  {
    cout<<"Read "<<landmarks.size()<<" points from "<< landmarkname<<endl;
    for (vector<Point>::iterator it = landmarks.begin() ; it != landmarks.end(); ++it)
      cout<<(*it).x<<"\t"<<(*it).y<<endl;
  }
  /*******************************************************************************************************/
  DelaunayFeatures del;
  del.loadDelaunayIndices(indicesname);
  del.plist = landmarks;

  cout<<"Angles"<<endl;
  del.calcDelaunayAngles();
  for(int i = 0; i < del.alist.size(); i++)
    cout<<del.alist[i][0]<<"\t"<<del.alist[i][1]<<"\t"<<del.alist[i][2]<<endl;


  /*******************************************************************************************************/
  return 0;
}
