#include <delaunay.h>

#include <iostream>
#include <cbcsl-utility.h>

using namespace cv;
using namespace std;


int display( Mat &img, int delay );

int main( int argc, char** argv )
{
  //Parse Arguments
  if(!(argc == 3 || argc == 4))
  {
    cout<<"Invalid Arguments\n\n";
    cout<<"./delaunay-test <image file> <landmark file>\n"<<"Sample use:\n./delaunay-test lena.jpg landmarks.dat\n";
    cout<<"\timage file\t- any commonly used image format .jpg, .png, .bmp, etc\n";
    cout<<"\tlandmarks file\t- each line fo the file should contain the point as \"x\ty\" without the inverted commas\n";
    cout<<"To Run in debug mode-"<<endl;
    cout<<"./delaunay-test <image file> <landmark file> d\n";
    return 0;
  }
  char * imagename = argv[1];
  char * landmarkname = argv[2];
  bool debug_mode = false;
  if(argc == 4)
    debug_mode = true;
  /*******************************************************************************************************/
  
  //Load files into memory
  Mat src = imread( imagename, CV_LOAD_IMAGE_GRAYSCALE);
  if(debug_mode)
    if(src.empty())
      cout<<"Unable to read:"<<imagename<<endl;
    else
      cout<<"Read "<<imagename<<" as GRAYSCALE"<<endl;

  vector<Point2f> landmarks = readLandmarkPoints(landmarkname);
  if(debug_mode)
    if(landmarks.empty())
    {
      cout<<"No Landmark Points read"<<endl;
      return 0;
    }

  if(debug_mode)
  {
    cout<<"Read "<<landmarks.size()<<" points from "<< landmarkname<<endl;
    for (vector<Point2f>::iterator it = landmarks.begin() ; it != landmarks.end(); ++it)
      cout<<(*it).x<<"\t"<<(*it).y<<endl;
  }
  /*******************************************************************************************************/
  string win_delaunay = "Delaunay Triangulation";

  DelaunayFeatures del(src, landmarks);
   if(debug_mode)
  {
    Mat img;
    del.drawDelaunayTriangles(src, img);
    imshow( win_delaunay, img);
    waitKey(0);
  }
  cout<<"Indices"<<endl;
  del.calcDelaunayIndices();
  for(int i = 0; i < del.ilist.size(); i++)
    cout<<del.ilist[i][0]<<"\t"<<del.ilist[i][1]<<"\t"<<del.ilist[i][2]<<endl;
  cout<<"Angles"<<endl;
  del.calcDelaunayAngles();
  for(int i = 0; i < del.alist.size(); i++)
    cout<<del.alist[i][0]<<"\t"<<del.alist[i][1]<<"\t"<<del.alist[i][2]<<endl;
  if(debug_mode)
  {
    Mat img;
    del.drawDelaunayTriangles(src, img);
    imshow( win_delaunay, img);
    waitKey(0);
  }


  /*******************************************************************************************************/
  return 0;
}
