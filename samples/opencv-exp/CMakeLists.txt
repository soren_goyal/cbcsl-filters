cmake_minimum_required(VERSION 2.8)
project( Filters )

add_executable( opencv-exp opencv-exp.cpp  )

target_link_libraries( opencv-exp ${OPENCV_LIBS} )
