/**
 * \mainpage Expression Classification - Computational Biology and Cognitive Sciences Lab
# Folder Structure #
- **doc**
  Contains files related to documentation
	
- **matlab-source**
  Contains the modified matlab source files and other scripts that were used to generate the data used for checking the correctness of the C++ code.

- **reference**
  Original data and matlab-source

- **samples**
  Sample codes that demonstrate how to use each class in the library. The description of each can be read in the Examples link

- **source**
  Contains all the source code

Build Instructions
==================
Set OpenCV Paths
----------------

In the CMakeLists.txt in the root folder of the project give the path of the OpenCV Libraries by setting the Cmake variables - OPENCV_INCLUDE_DIR, OPENCV_LINK_DIR, OPENCV_LIBS.

Build the project using CMAKE
-----------------------------

	mkdir build
	cd build
	cmake ..
	make

Write your own code
-------------------

The quickest way to write your own code would be to compy a sample folder and make edit to it

# Optimizations #
In cbcsl-utility.cpp, the function getFace() modifies the landmarks but does not write back the changes. Instead the adjustLandmarks() function is used. They can be merged.

In the following functions the C++ data structure is either returned or copied while allocation, this can be changed

In cbcsl-utility.cpp, the CSVToMat() can be made a lot more efficient.

cbcsl-utility.h has a nice function of reading csv file into a opencv Mat. This can be used in place of functions like readLandmarkPoints() and loadDelaunayIndices(char *filename)

In many places, over the code data is treated as double or CV_64F, this should be replaced with float or CV_32F.

In the gabor-features.cpp, the function applyFilters() can be made a lot faster. The gabor filters are being applied to all the points in the image. Instead they can be apllied to only the landmark points. But this will reuire you to type in code to explicitly multiply each image pixel and filter pixel.
\section Bugs
In classifier.cpp, in the set() functions only the references are being assigned, the data is not being copied.
*/
