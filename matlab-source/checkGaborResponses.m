%Code to check if the responses of the matlab's GaborFeaturesFunc_ver2() is
%same as the one implemented in the c++

%%
I =imread('test_face.jpg');
markings = dlmread('test_face.land', '\t');
G = runGaborFeatureExtraction(I,markings);
G=G';
for p=1:66
    for f=1:40
        oldresponses(p,f,1) = G((p-1)*40*2 + 2*(f-1) + 1);
        oldresponses(p,f,2) = G((p-1)*40*2 + 2*(f-1) + 2);
    end
end
%%
F = dlmread('test_face_gabor_responses.txt');
for p=1:66 %66 points
    for f=1:40 %40 filters per point - one real and one imaginery
        newresponses(p,f,1) = F((p-1)*40*2 + 2*(f-1) + 1);
        newresponses(p,f,2) = F((p-1)*40*2 + 2*(f-1) + 2);
    end
end
%%
for p=1:66
    rmsmod(p,1) = rms(oldresponses(p,:,1));
    rmsmod(p,2) = rms(newresponses(p,:,1));
    rmsphase(p,1) = rms(oldresponses(p,:,2));
    rmsphase(p,2) = rms(newresponses(p,:,2));
end

%%
% 
% figure 
% hist(F,30);
% 
% figure
% hist(G,30);

fig1 = figure();
x = 1:40
for point = 1:66


figure(fig1)
%hist(oldresponses(point,:,2),10);
scatter(x, oldresponses(point,:,1));
hold on
scatter(x, newresponses(point,:,1));
hold off
pause()
end

