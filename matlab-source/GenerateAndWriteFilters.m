function [filterBank] = GenerateFilters()

%Same as GenerateFilters, but it writes in the same order as the filters
%are created, with the appropriate file names
%% Parameters

theta = (0:7)*pi/8;                       % Orientation
lamda = [4 4*sqrt(2) 8 8*sqrt(2) 16]; % Wavelength
phi = [0 pi/2];                           % Phase
sigma = 3*lamda/4;                        % Gaussian radius
gamma = 1;                                % Aspect ratio

N = 75;          % Size of the filter
Nd = floor(N/2); % Diameter of the filter

for p=1:length(phi)
    rows=0;
    for t=1:length(theta)
        for l=1:length(lamda)
            W=[];
            for x=-Nd:Nd
                for y=-Nd:Nd
                    a = x*cos(theta(t)) + y*sin(theta(t));
                    b = -x*sin(theta(t)) + y*cos(theta(t));
                    
                    r = a^2+ (gamma*b)^2;
                    W(x+Nd+1,y+Nd+1) = exp(-r/(2*sigma(l)^2))*cos(2*pi*(a/lamda(l)) + phi(p));
                end;
            end;
             %imagesc(W);%, colormap gray;
             %pause,
            rows=rows+1;
            filterBank{p}(:,:,rows) = W;
            name = strcat('t',num2str(t),'l',num2str(l),'p',num2str(p),'.csv');
            csvwrite(name,W);
            
        end;
    end;
end;