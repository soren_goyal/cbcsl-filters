testingdata = csvread('reference/data/sample-classification-model/testingdata.txt');
C = csvread('reference/data/sample-classification-model/C.txt');
nc = csvread('reference/data/sample-classification-model/nc.txt');
op_sigma = csvread('reference/data/sample-classification-model/op_sigma.txt');
trainingdata = csvread('reference/data/sample-classification-model/trainingdata.txt');
v = csvread('reference/data/sample-classification-model/v.txt');
K1 = csvread('reference/data/sample-classification-model/K1.txt');
%%
testingdata = testingdata(:,1:2);
test_label = [0,0];

%%
train=v'*K1;
training2 = full(dot(trainingdata,trainingdata,1));
testing2 = full(dot(testingdata,testingdata,1))';
testtrain = full(2*(testingdata'*trainingdata));
bsxwala = bsxfun(@plus,training2, testing2);
dd = bsxwala -testtrain;
dd=dd';
K2=exp(-dd/(2*op_sigma^2));

test=v'*K2;

%%
csvwrite('reference/data/classification-model-test-parameters/testingdata.txt',testingdata);
csvwrite('reference/data/classification-model-test-parameters/dd.txt',dd);
csvwrite('reference/data/classification-model-test-parameters/trainingdata.txt',trainingdata);
csvwrite('reference/data/classification-model-test-parameters/op_sigma.txt',op_sigma);
csvwrite('reference/data/classification-model-test-parameters/v.txt',v);
csvwrite('reference/data/classification-model-test-parameters/C.txt',C);
csvwrite('reference/data/classification-model-test-parameters/nc.txt',nc);
csvwrite('reference/data/classification-model-test-parameters/K1.txt',K1);
csvwrite('reference/data/classification-model-test-parameters/K2.txt',K2);
%%
[rate classEstimate ]=NearestNeighbor(train',test',test_label,C,nc);