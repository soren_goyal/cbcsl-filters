function [G] = runGaborFeatureExtraction(I,markings)

useful_markings = 1:66;
G=zeros(1,5280);

G_sample = GaborFeaturesFunc_ver2(I, markings, useful_markings);

if size(G_sample,1)==5280 

        G=G_sample';
            
end
    

