function [] = converMatToSimple(filename)
load(strcat(filename,'.mat'));
dlmwrite(strcat(filename,'.land'),faceCoordinatesUnwarped,'\t');