#include <classifier.h>
#include <assert.h>
#include <vector>
#include <string>

using namespace std;
using namespace cv;

Classifier::Classifier()
{

}
Mat Classifier::classify(Mat &unknownvectors)
{
	assert(this->v.rows == this->K1.cols);
	Mat train = this->v.t()*this->K1;
	assert(train.rows == v.cols && train.cols == K1.cols);

	vector<float> norm_labelledvectors, norm_unknownvectors;
	for (int i = 0; i < labelledvectors.cols; i++)
	{
		norm_labelledvectors.push_back(
						labelledvectors(Range::all(), Range(i,i+1)).dot(
							labelledvectors(Range::all(), Range(i,i+1)))
								);
	}
	assert(norm_labelledvectors.size() == labelledvectors.cols);
	cout<<"norm_labelledvectors created"<<endl;
	for (int i = 0; i < unknownvectors.cols; i++)
	{
		norm_unknownvectors.push_back(
						unknownvectors(Range::all(), Range(i,i+1)).dot(
							unknownvectors(Range::all(), Range(i,i+1)))
								);
	}
	assert(norm_unknownvectors.size() == unknownvectors.cols);
	cout<<"norm_unknownvectors created"<<endl;
	this->dd.create(norm_unknownvectors.size(), norm_labelledvectors.size(), CV_32F);
	for(int i = 0; i < norm_unknownvectors.size(); i++)
		for(int j = 0; j < norm_labelledvectors.size(); j++)
			this->dd.at<float>(i,j) = norm_unknownvectors[i]+norm_labelledvectors[j];
	cout<<"bsx_fun done"<<endl;
	Mat temp = unknownvectors.t()*labelledvectors;
	temp = temp*2;
	this->dd = this->dd - temp;
	assert(dd.rows == unknownvectors.cols && dd.cols == labelledvectors.cols);
	this->dd = this->dd.t();
	this->K2.create(this->dd.size(), CV_32F);
	cout<<"dd computed"<<endl;
	exp(-this->dd/(2*op_sigma*op_sigma),K2);
	cout<<"K2 computed created"<<endl;
	Mat test = this->v.t()*K2;
	train = train.t();
	test = test.t();
	Mat predictions = getNearestNeighborClass(train, test);
	return predictions;
}
Mat  Classifier::getNearestNeighborClass(Mat &train, Mat &test)
{
	assert(train.cols == test.cols );
	assert(test.rows > 0);

	Mat predictions(test.rows, 1, CV_32S);
	for(int n = 0; n < test.rows; n++)
	{
		float min_distance = 3e38; //Maximum possible value of float
		int nn_index = 0; //index of nearest neighbor
	
		float diff;
		//Find the nearest nighbor for each n
		for (int m = 0; m < train.rows; ++m)
		{
			float distance = 0;
			for (int i = 0; i < train.cols; ++i)
			{
				diff = test.at<float>(n,i) - train.at<float>(m,i);
				distance = distance + diff*diff;
			}
			if(min_distance > distance)
			{
				min_distance = distance;
				nn_index = m;
			}
		}
		//Check the class of the index
		for(int i = 0; i < nc.cols; i++)
		{
			if(nn_index < nc.at<int>(0,i))
			{
				predictions.at<int>(n,0) = i+1;
				break;
			}
			else
				nn_index -= nc.at<int>(0,i);
		}
	}
	return predictions;	
}

void Classifier::setV(cv::Mat &v)
{
	v.copyTo(this->v);
}
void Classifier::setC(int c)
{
	this->c = c;
}
void Classifier::setNC(cv::Mat &nc)
{
	nc.copyTo(this->nc);
}
void Classifier::setK1(cv::Mat &K1)
{
	K1.copyTo(this->K1);
}
void Classifier::setOpSigma(float op_sigma)
{
	this->op_sigma = op_sigma;
}
void Classifier::setLabelledVectors(cv::Mat &labelledvectors)
{
	labelledvectors.copyTo(this->labelledvectors);
}