/**
 * @file distances.h
 * Functions to compute eulidean distances between all the feature points
 */
#ifndef _DISTANCES_H_
#define _DISTANCES_H_

#include <vector>
#include "opencv2/opencv.hpp"

/**
 * @fn std::vector<double> getEuclideanDistances(std::vector<cv::Point> landmarks)
 * @param landmars list of feature points
 * @return A list of Euclidean Distances  
 */
std::vector<float> getEuclideanDistances(std::vector<cv::Point2f> landmarks);

#endif