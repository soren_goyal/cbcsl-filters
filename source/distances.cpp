#include <distances.h>
#include <cbcsl-utility.h>
#include <math.h>


using namespace cv;
using namespace std;

vector<float> getEuclideanDistances(vector<Point2f> landmarks)
{
	vector<float> distances;
	for(int i = 1; i < landmarks.size(); i++)
		for(int j = 0; j < i; j++)
		{
			Point d = landmarks[i] - landmarks[j];
			distances.push_back(norm(d));
		}
	return distances;
}