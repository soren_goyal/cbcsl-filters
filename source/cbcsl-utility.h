/**
 * @file cbcsl-utility.h
 * Contains useful functions for I/O and image pre-processing
 */
#ifndef _CBCSL_UTILITY_H_
#define _CBCSL_UTILITY_H_

#include <vector>
#include <string>
#include "opencv2/opencv.hpp"

#define LEFT_EYE 19 /*!< The index of the landmark point that sits on the left corner of the left eye. */
#define RIGHT_EYE 28/*!< The index of the landmark point that sits on the right corner of he right eye. */
#define LEFT_EYEBROW 3 /*!< The index of the landmark point that sits on the middle of the left eyebrow. */
#define RIGHT_EYEBROW 8/*!< The index of the landmark point that sits on the middle of the right eyebrow. */
#define LEFT_LIP 58 /*!< The index of the landmark point that sits on the left corner fo the lips. */
#define RIGHT_LIP 59 /*!< The index of the landmark point that sits on the right corner fo the lips. */
#define LEFT_EXTREME 51 /*!< The index of the leftmost landmark point. */
#define RIGHT_EXTREME 65 /*!< The index of the rightmost landmark point. */
#define MEAN_EYE_DISTANCE 200 /*!< The getFace() resizes the faces such that the distance between the eyes is equal to the MEAN_EYE_DISTANCE*/

/*!
\fn std::vector<cv::Point> readLandmarkPoints(char *filename)
 \brief Reads the landmark points from a file.

 Each line of the file should contain a point in the format: x y.
 A sample file containing landmark points is in the folder references/data/image-data/. The extension of the file is \a .land
  
  \param filename Name of the file to be read
  \return vector of Opencv struct Point containing the landmark points
*/
std::vector<cv::Point2f> readLandmarkPoints(char *filename);

/*!
@fn cv::Mat readCSVToMat(char *filename);
 @brief Reads doubel values from a CSV File into a OpenCV Mat  
  @param filename Name of the file to be read
  @return {Matrix in the form of OpenCV Mat. The matrix returned is empty of the program is unable to read the file.}

*/
void readCSVToMat(cv::Mat &mat, char *filename, int type = CV_32F);
/**
 * @overload cv::Mat readCSVToMat(std::string filename)
 */
void readCSVToMat(cv::Mat &mat, std::string filename, int type = CV_32F);



/*!
\fn void visualizePointsOnImage(cv::Mat img, std::vector<cv::Point2f> landmarks)
\brief Displays the landmark points on the image

\param img Image to be displayed
\param landmarks vector of OpenCV points

\return nothing
*/
void visualizePointsOnImage(cv::Mat img, std::vector<cv::Point2f> landmarks);

/*!
\fn void getFace(cv::Mat &src, cv::Mat &dst, std::vector<cv::Point> &landmarkpoints)
Crops out a face from an image, resizes and rotates it so that the line joining the eyes is at 0 degrees

The landmarks points are not affected. To modify them such that they fit the new image use adjustLandmarks() !

\warning This does not use a face detection algorithm but instead uses the landmark points to create a bounding box around the face.
\param src source image from which the face is to be cropped
\param dst image where the croped and rotated face is stored
\param landmarkpoints vector of OpenCV points

\return nothing

\see adjustLandmarks()
\see MEAN_EYE_DISTANCE
*/
void getFace(cv::Mat &src, cv::Mat &dst, std::vector<cv::Point2f> &landmarkpoints);

/*!
@fn void adjustLandmarks(std::vector<cv::Point> &landmarks,vector<Point> &newlandmarks )
Rotates the landmarks points so that they fit onto the cropped, resized and rotated face.

@param landmarks vector of OpenCV points
@param newlandmarks vector where the new points are written

@returns Nothing

@see getFace()
*/
void adjustLandmarks(std::vector<cv::Point2f> &landmarks, std::vector<cv::Point2f> &newlandmarks );


/**
 * @fn bool compareMat(cv::Mat A, cv::Mat B)
 * Used to comapre if two matrices containing double elements.
 * @param A matrix
 * @param B matrix
 * @return Returns true if the matrices are identical
 */
 bool compareMat(cv::Mat A, cv::Mat B);


#endif 

