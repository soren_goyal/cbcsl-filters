#include <cbcsl-utility.h>
#include <fstream>
#include <iostream>
#include <math.h>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <assert.h>

using namespace cv;
using namespace std;

vector<Point2f> readLandmarkPoints(char *filename)
{
	std::vector<Point2f> landmarks;
	string line;
	ifstream file(filename, ios::in);
	if (file.is_open())
	{
		while ( getline (file,line) )
		{
		  if(line.size() <= 1)
		    continue;
		  float x,y;
		  sscanf(line.c_str(),"%f\t%f", &x, &y);
		  landmarks.push_back(Point2f(x,y));
		}
		file.close();
	}
	return landmarks;
}
void readCSVToMat(Mat &mat, string filename, int type)
{
	char * c_filename = new char[filename.size()];
	strcpy (c_filename, filename.c_str());
	readCSVToMat(mat, c_filename, type);
}

void readCSVToMat(Mat &mat, char *filename, int type)
{
	string line;
	string delim = ",";
	string token;
	ifstream file(filename, ios::in) ;
	float f;
	vector<float> fvec;
	int x;
	vector<int> ivec;
	int rows = 0;
	if (file.is_open())
	{
		
		while ( getline (file,line, '\n') )
		{
			if(line.size() < 1) //If line is empty
		    	continue;
			line = line + ',';
			rows++;			
			while(!line.empty())
			{
				token = line.substr(0, line.find(delim));
				line.erase(0, line.find(delim) + delim.size());
				switch(type)
				{
					case CV_32F:sscanf(token.c_str(),"%f", &f);
								fvec.push_back(f);
								break;
					case CV_32S:sscanf(token.c_str(),"%d", &x);
								ivec.push_back(x);
								break;
				}
			}
		}
	}
	switch(type)
	{
		case CV_32F:mat = Mat(fvec, true).reshape(1,rows);
					break;
		case CV_32S:mat = Mat(ivec, true).reshape(1,rows);
					break;
	}
	file.close();
}

void visualizePointsOnImage(Mat img, vector<Point2f> landmarks)
{
	Mat temp = img.clone();
    cvtColor(temp, temp, CV_GRAY2BGR);
    for (vector<Point2f>::iterator it = landmarks.begin() ; it != landmarks.end(); ++it)
      circle(temp, *it, 3, Scalar(51,255,255));//, int thickness=1, int lineType=8, int shift=0);
    imshow( "Image with Points", temp );
    int c = waitKey ( 0000 );
	if( c >= 0 ) return;
}

void getFace(Mat &src, Mat &dst, vector<Point2f> &landmarks_original)
{
	vector<Point2f> landmarks(landmarks_original);
	Point2f eye_vector = landmarks[RIGHT_EYE] - landmarks[LEFT_EYE];
	float eye_distance = norm(eye_vector);
	int ylim_upper, ylim_lower, xlim_left, xlim_right;
	int lim;
	lim = (landmarks[LEFT_EYEBROW].y + landmarks[RIGHT_EYEBROW].y)/2 - eye_distance;
	ylim_upper = lim<0 ? 0 : lim;
	lim = (landmarks[LEFT_LIP].y + landmarks[RIGHT_LIP].y)/2 + eye_distance/2;
	ylim_lower = lim>src.rows ? src.rows : lim;
	lim = landmarks[LEFT_EXTREME].x - eye_distance/3;
	xlim_left = lim<0 ? 0 : lim;
	lim = landmarks[RIGHT_EXTREME].x + eye_distance/3;
	xlim_right = lim>src.cols ? src.cols : lim;
	Mat temp(src, Rect(xlim_left, ylim_upper, xlim_right - xlim_left, ylim_lower - ylim_upper)); //roi(x,y,height,width)
	dst = temp.clone(); //Remove this later
	for (int i = 0; i < landmarks.size(); ++i)
	{
		landmarks[i].x -= xlim_left;
		landmarks[i].y -= ylim_upper;
	}
	/* Comments on warpAffine() and getRotationMatrix2D()
	 * getRotationMatrix2D() gives a rotation matrix of the form -
	 * [ cos(t) sin(t) (1-cos(t))*X - sin(t)*Y ]
	 * [-sin(t) cos(t) sin(t)*X + (1-cos(t))*Y ]
	 * where (X,Y) is the center of rotation. (X = column number, Y = row number)
	 * If this matrix is passed into warpAffine() it will rotate the image about the (X,Y) by an agngle 't' counter-clockwise
	 * The center will stay at the same point in the image.
	 * Also the type of the rot_mat will be CV_64F or <double>
	*/
	//Scale Up
	float scale = MEAN_EYE_DISTANCE/eye_distance;
	Mat rot_mat = getRotationMatrix2D( Point(0,0), 0, scale ); //access of rot_mat(2,3) -> [i,j] =  .at<float>(i,j)
	rot_mat.convertTo(rot_mat, CV_32F);
	int width = temp.cols*scale, height = temp.rows*scale;
	warpAffine( temp, dst, rot_mat, Size(width, height), CV_INTER_LINEAR );
	for (vector<Point2f>::iterator it = landmarks.begin() ; it != landmarks.end(); ++it)
    {
    	float x  = (*it).x, y = (*it).y;
    	(*it).x = x*rot_mat.at<float>(0,0) + y*rot_mat.at<float>(1,0) + rot_mat.at<float>(2,0) ;
    	(*it).y = x*rot_mat.at<float>(0,1) + y*rot_mat.at<float>(1,1) + rot_mat.at<float>(2,1) ;
    }
    //Rotate
	float angle_image = -180.0*atan2(-1*eye_vector.y, eye_vector.x)/CV_PI;
	rot_mat = getRotationMatrix2D( landmarks[LEFT_EYE], angle_image, 1 ); //access of rot_mat(2,3) -> [i,j] =  .at<float>(i,j)
	rot_mat.convertTo(rot_mat, CV_32F);
	warpAffine( dst, dst, rot_mat, dst.size(), CV_INTER_LINEAR, BORDER_REFLECT);
}
void adjustLandmarks(vector<Point2f> &landmarks,vector<Point2f> &newlandmarks)
{
	Point2f eye_vector = landmarks[RIGHT_EYE] - landmarks[LEFT_EYE];
	float eye_distance = norm(eye_vector);
	//Re-position
	int ylim_upper, ylim_lower, xlim_left, xlim_right;
	int lim;
	lim = (landmarks[LEFT_EYEBROW].y + landmarks[RIGHT_EYEBROW].y)/2 - eye_distance;
	ylim_upper = lim<0 ? 0 : lim;
	lim = landmarks[LEFT_EXTREME].x - eye_distance/3;
	xlim_left = lim<0 ? 0 : lim;
	for (int i = 0; i < landmarks.size(); ++i)
	{
		landmarks[i].x -= xlim_left;
		landmarks[i].y -= ylim_upper;
	}
	
	//Scale
	float scale = MEAN_EYE_DISTANCE/eye_distance;
	Mat rot_mat = getRotationMatrix2D( Point(0,0), 0, scale ); //access of rot_mat(2,3) -> [i,j] =  .at<float>(i,j)
	rot_mat.convertTo(rot_mat, CV_32F);
	for (vector<Point2f>::iterator it = landmarks.begin() ; it != landmarks.end(); ++it)
    {
    	float x  = (*it).x, y = (*it).y;
    	(*it).x = x*rot_mat.at<float>(0,0) + y*rot_mat.at<float>(1,0) + rot_mat.at<float>(2,0) ;
    	(*it).y = x*rot_mat.at<float>(0,1) + y*rot_mat.at<float>(1,1) + rot_mat.at<float>(2,1) ;
    }
    //Rotate
    float angle_image = -180.0*atan2(-1*eye_vector.y, eye_vector.x)/CV_PI;
	rot_mat = getRotationMatrix2D( landmarks[LEFT_EYE], angle_image, 1 ); //access of rot_mat(2,3) -> [i,j] =  .at<float>(i,j)
	rot_mat.convertTo(rot_mat, CV_32F);
	for (vector<Point2f>::iterator it = landmarks.begin() ; it != landmarks.end(); ++it)
    {
    	float x  = (*it).x, y = (*it).y;
    	(*it).x = x*rot_mat.at<float>(0,0) + y*rot_mat.at<float>(0,1) + rot_mat.at<float>(0,2);
    	(*it).y = x*rot_mat.at<float>(1,0) + y*rot_mat.at<float>(1,1) + rot_mat.at<float>(1,2);
    }
    newlandmarks = landmarks;
}

bool compareMat(cv::Mat A, cv::Mat B)
{
 	if(A.rows != B.rows || A.cols != B.cols)
 		return false;
 	for(int i = 0; i < A.rows; i++)
 		for(int j = 0; j < A.cols; j++)
 			if( !(A.at<float>(i,j) >= B.at<float>(i,j) - 0.0001 && A.at<float>(i,j) <= B.at<float>(i,j) + 0.0001) )
				return false; 				
 	return true;
 }
