#include <gabor-features.h>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <cbcsl-utility.h>
#include <stdio.h>

using namespace std;
using namespace cv;

GaborFeatures::GaborFeatures()
{
	//Set Gabor Kernel parmeters
	theta.resize(8);
	for(int i = 0; i < 8; i++)
		theta[i] = -((float)i)*CV_PI/8.0 - CV_PI/2.0; //Angle values have been corrected so that the filter generated match the matlab generated filters
	lambda.resize(5);
	sigma.resize(5);
	for(int i = 0; i < 5; i++)
	{
		lambda[i] = 4*pow(sqrt(2),i); 
		sigma[i] = 3*pow(sqrt(2),i); 
	}
	psi.resize(2);
	psi[0] = 0; psi[1] = CV_PI/2.0;
	gamma.push_back(1);
	max_kernel_length = 75;
}
int GaborFeatures::readFilters(char * foldername)
{
	char filename[100];
	for(int t = 0; t < theta.size(); t++)
			for(int l = 0; l < lambda.size(); l++)
				for(int p = 0; p < psi.size(); p++)
				{	sprintf(filename,"%s/t%dl%dp%d.csv",foldername, t+1, l+1, p+1); //Coz matlab has indices from 1 to n rather than 0 to n-1
					Mat mat;
					readCSVToMat(mat, filename);
					if(mat.empty())
					{
						filters.clear();
						return 0;
					}
					filters.push_back(mat);
				}
	return filters.size();
}
int GaborFeatures::generateFilters()
{
	for(int t = 0; t < theta.size(); t++)
		for(int l = 0; l < lambda.size(); l++)
			for(int p = 0; p < psi.size(); p++)
				filters.push_back(getGaborKernel(Size(max_kernel_length,max_kernel_length), sigma[l], theta[t], lambda[l], gamma[0], psi[p], CV_32F));
	return filters.size();
}

int GaborFeatures::applyFilters(Mat img, vector<Point2f> landmarks)
{
	if(filters.empty())
		return 0;
	responses.resize(filters.size()*landmarks.size());
	Mat destr, desti; //For storing Real and Imaginery parts
 	img.convertTo(img, CV_32F);
  for(int f = 0; f < filters.size(); f += 2 )
	  {
  	//Spare me for being lazy, but I am applying the gabor filtering to all pixels
    filter2D(img, destr, CV_32F, filters[f], Point(-1,-1), 0, BORDER_REFLECT);
    filter2D(img, desti, CV_32F, filters[f+1], Point(-1,-1), 0, BORDER_REFLECT);
 //    Mat tempr, tempi;
 //    destr.convertTo(tempr,CV_8U);
 //    desti.convertTo(tempi,CV_8U);
 // //    imshow("real", tempr);
 //    imshow("imaginery", tempi);
 //    cout<<filters[f+1];
 //    cout<<"Real="<<destr.at<double>(landmarks[0]);
	// cout<<"Imag="<<desti.at<double>(landmarks[0])<<endl;
 //    int c = waitKey ( 0 );
    for(int n = 0; n < landmarks.size(); n++)    
    {
      responses[n*filters.size() + f] = sqrt(pow(destr.at<float>(landmarks[n]),2) + pow(desti.at<float>(landmarks[n]),2));
      responses[n*filters.size() + f+1] = atan2( desti.at<float>(landmarks[n]), destr.at<float>(landmarks[n]) );
    }
  }
  return responses.size();
}