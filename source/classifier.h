/**
 * @file classifier.h
 * Contains useful functions for I/O and image pre-processing
 */
#ifndef _CLASSIFIER_H_
#define _CLASSIFIER_H_

#include <delaunay.h>
#include <distances.h>
#include <gabor-features.h>
#include "opencv2/opencv.hpp"


/**
 * @fn std::vector<cv::Point> readLandmarkPoints(char *filename)
 * @brief Reads the landmark points from a file.

 Each line of the file should contain a point in the format: x y.
 A sample file containing landmark points is in the folder references/data/image-data/. The extension of the file is \a .land
  
  @param filename Name of the file to be read
  @return vector of Opencv struct Point containing the landmark points
*/
class Classifier
{
public:
	cv::Mat v;
	int c;
	cv::Mat nc;
	cv::Mat K1;
	float op_sigma;
	cv::Mat dd;
	cv::Mat K2;
	cv::Mat labelledvectors;/*!< Stores the feature vectors with known labels. Dimensions:  Number of Features x Number of vectors */
	
public:
	Classifier();
	void setV(cv::Mat &v);
	void setC(int c);
	void setNC(cv::Mat &nc);
	void setK1(cv::Mat &K1);
	void setOpSigma(float op_sigma);
	void setLabelledVectors(cv::Mat &labelledvectors);
	cv::Mat classify(cv::Mat &unknownvectors);
	cv::Mat getNearestNeighborClass(cv::Mat &train, cv::Mat &test);
};

#endif 

