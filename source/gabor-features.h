/**
 * @example gabor-features-test.cpp
 * Input - image file and landmark files
 * Generates the gabor filters, applies the filters to the image and prints the responses
 */
#ifndef _GABOR_FEATURES_H_
#define _GABOR_FEATURES_H_

#include "opencv2/opencv.hpp"
#include <vector>

/**
 * @brief Class for computing the Gabor Responses at the features points
 */
class GaborFeatures
{
public:
	/**
	 * @fn GaborFeatures(); 
	 * @details The value of the parameters is set in the constructor and is hard coded. These parameters are used by the functions readFilters() and generateFilters()
	 */
	GaborFeatures();
	/**
	 * @fn readFilters(char * foldername)
	 * @details Read filters from the folder. The folder should be of a specific format.
	 * A sample folder is - <project source>/reference/data/gabor-filters
	 * 
	 * @param foldername Name of the folder
	 * @return Number of filters read
	 */
	int readFilters(char * foldername);
	/**
	 * @fn generateFilters()
	 * @details Generates the filters as per the parameters initialized in GaborFeatures().
	 * The [filters](@ref filters) is populated by this function.
	 * @return Number of filters generated
	 */
	int generateFilters();
	/**
	 * @details Apply Filters to Land mark Points in the image	
	 * 
	 * @param img Image
	 * @param landmarks landmark points
	 * 
	 * @return Total numebr of responses generated
	 */
	int applyFilters(cv::Mat img, std::vector<cv::Point2f> landmarks); 
	std::vector<float> theta; /*!< Orientation Intialized to i*PI/8 */
	std::vector<float> lambda;/*!< Wavelength Initialized to */
	std::vector<float> sigma; /*!<Gaussian radius Initialized to */
	std::vector<float> psi;
	std::vector<float> gamma; /*!< Aspect Ratio Intialized to 1 */
	int max_kernel_length;

	std::vector<cv::Mat> filters;
	std::vector<float> responses;
	
	
};

#endif//_GABOR_FEATURES_H_