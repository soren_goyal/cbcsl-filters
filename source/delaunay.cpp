#include <delaunay.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <fstream>
#include <assert.h>

using namespace cv;
using namespace std;

DelaunayFeatures::DelaunayFeatures() {

}
DelaunayFeatures::DelaunayFeatures(Mat src, vector<Point2f> landmarks)    {
    img = src.clone();
    plist = landmarks;
    
    Size size = img.size();
    Rect rect(0, 0, size.width, size.height);
    subdiv.initDelaunay(rect);
    for( vector<Point2f>::iterator it = plist.begin(); it != plist.end(); it++)
        subdiv.insert(*it);
}


void DelaunayFeatures::calcDelaunayIndices() {
    if(!(ilist.empty()))
        ilist.clear();

    vector<Vec6f> triangleList;
    subdiv.getTriangleList(triangleList);
    Size size = img.size();
    Rect rect(0,0, size.width, size.height);

    for(size_t i = 0; i < triangleList.size(); i++)
    {
        Vec6f t = triangleList[i];
        if(!rect.contains(Point2f(t[0],t[1])) || !rect.contains(Point2f(t[2],t[3])) || !rect.contains(Point2f(t[4],t[5])))
            continue; //Ignore the triangles with points lying outside the image
        vector<int> triangle;
        for(int j = 0; j < plist.size(); j++)
            if(t[0] == plist[j].x && t[1] == plist[j].y)
            {
                triangle.push_back(j);
                break;
            }
        assert(triangle.size() == 1);
        for(int j = 0; j < plist.size(); j++)
            if(t[2] == plist[j].x && t[3] == plist[j].y)
            {
                triangle.push_back(j);
                break;
            }
        assert(triangle.size() == 2);
        for(int j = 0; j < plist.size(); j++)
            if(t[4] == plist[j].x && t[5] == plist[j].y)
            {
                triangle.push_back(j);
                break;
            }
        assert(triangle.size() == 3);
        ilist.push_back(triangle);
    }
}

void DelaunayFeatures::loadDelaunayIndices(char *filename) {
    string line;
    ifstream file(filename, ios::in) ;
    if (file.is_open())
    {
        while ( getline (file,line) )
        {
          if(line.size() <= 1)
            continue;
          int a, b, c;
          sscanf(line.c_str(),"%d\t%d\t%d", &a, &b, &c);
          vector<int> v;
          v.push_back(a);
          v.push_back(b);
          v.push_back(c);
          ilist.push_back(v);
        }
        file.close();
    }
}
void DelaunayFeatures::calcDelaunayAngles() {
    vector<Vec6f> triangleList;
    subdiv.getTriangleList(triangleList);
    Size size = img.size();
    Rect rect(0,0, size.width, size.height);
    for(size_t i = 0; i < triangleList.size(); i++)
    {
         Vec6f t = triangleList[i];
        if(!rect.contains(Point2f(t[0],t[1])) || !rect.contains(Point2f(t[2],t[3])) || !rect.contains(Point2f(t[4],t[5])))
            continue; //Ignore the triangles with points lying outside the image
        vector<float> angles;
        angles.push_back(calcAngle(Point2f(t[0],t[1]),Point2f(t[2],t[3])));
        angles.push_back(calcAngle(Point2f(t[2],t[3]),Point2f(t[4],t[5])));
        angles.push_back(calcAngle(Point2f(t[4],t[5]),Point2f(t[0],t[1])));
        alist.push_back(angles);
    }

}

double DelaunayFeatures::calcAngle(Point2f a, Point2f b)
{
    return acos((a.x*b.x + a.y*b.y)/norm(a)/norm(b));
}

void DelaunayFeatures::drawDelaunayTriangles(Mat &src, Mat &dst)  {
    Scalar delaunay_color(255,255,255), points_color(0, 0, 255); 
    vector<Vec6f> triangleList;
    subdiv.getTriangleList(triangleList);
    vector<Point> pt(3);
    Size size = src.size();
    Rect rect(0,0, size.width, size.height);
 
    dst = src.clone();
    for( size_t i = 0; i < triangleList.size(); i++ )
    {
        Vec6f t = triangleList[i];
        pt[0] = Point(cvRound(t[0]), cvRound(t[1]));
        pt[1] = Point(cvRound(t[2]), cvRound(t[3]));
        pt[2] = Point(cvRound(t[4]), cvRound(t[5]));
         
        // Draw rectangles completely inside the image.
        if ( rect.contains(pt[0]) && rect.contains(pt[1]) && rect.contains(pt[2]))
        {
            line(dst, pt[0], pt[1], delaunay_color, 1, CV_AA, 0);
            line(dst, pt[1], pt[2], delaunay_color, 1, CV_AA, 0);
            line(dst, pt[2], pt[0], delaunay_color, 1, CV_AA, 0);
        }
    }
    for( vector<Point2f>::iterator it = plist.begin(); it != plist.end(); it++)
      circle(dst, *it, 2, points_color, CV_FILLED, CV_AA, 0);
}