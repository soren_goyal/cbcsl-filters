
/**
 * @example delaunay-test.cpp
 * @example delaunay-test-2.cpp
 */
 
#ifndef _DELAUNAY_H_
#define _DELAUNAY_H_

#include "opencv2/opencv.hpp"

/**
 * @brief Contains useful functions for computing Delaunay Triangles and their angles
 */
class DelaunayFeatures
{
public:	
	cv::Mat img;
	cv::Subdiv2D subdiv;
	std::vector<cv::Point2f> plist; /*!< List of Landmark points */
	std::vector<std::vector<int> > ilist; /*!< Vector of indices of Points of Delaunay Triangles. It is populated by void calcDelaunayIndices() */
	std::vector<std::vector<float> > alist; /*!< Vector of indices of Angles of Delaunay Triangles. It is populated by void calcDelaunayAngles() */

	DelaunayFeatures();
	DelaunayFeatures(cv::Mat img, std::vector<cv::Point2f> landmarks);
	/**
	 * @fn void calcDelaunayIndices();
	 * @brief Finds out the indices of the points that form a delaunay triangle. 
	 * It uses the std::vector<cv::Point> plist. It dumps these indices into std::vector<std::vector<int> > ilist, a vector of dimension Number of Triangles x 3.
	 *  
	 * @param none
	 * @return none
	 * @sa std::vector<cv::Point> plist and std::vector<std::vector<int> > ilist
	 */
	void calcDelaunayIndices();
	
	void loadDelaunayIndices(char *filename); //TODO:Not Checked
	
	/**
	 * @fn void calcDelaunayAngles()
	 * @brief Finds out the each of the three angles of Delaunay Triangles. 
	 * It uses the std::vector<cv::Point> plist. It dumps the angles into std::vector<std::vector<int> > alist, a vector of dimension Number of Triangles x 3.
	 *  
	 * @param none
	 * @return none
	 * @sa std::vector<cv::Point> plist and std::vector<std::vector<int> > alist
	 */
	void calcDelaunayAngles();
	/**
	 * @fn double calcAngle(cv::Point2f a, cv::Point2f b)
	 * @brief Computes angle between two vectors 
	 * 
	 *  
	 * @param a first vector
	 * @param b second vector
	 * @return angle in radians
	 */
	double calcAngle(cv::Point2f a, cv::Point2f b);
	/**
	 * @fn cv::Mat drawDelaunayTriangles()
	 * @brief Draws the delaunay triangles on the given image 
	 * The triangles are drawn on img using the points in plist.
	 *  
	 * @return Image with delaunay Triangles
	 * @sa cv::Mat img, std::vector<cv::Point> plist
	 */
	void drawDelaunayTriangles(cv::Mat &src, cv::Mat &dst);

};
#endif